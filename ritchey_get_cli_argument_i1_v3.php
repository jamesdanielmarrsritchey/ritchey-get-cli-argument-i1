<?php
#Name:Ritchey Get CLI Argument i1 v3
#Description:Get value of commandline argument. Returns argument value (meaning the next argument) on success. Returns "NULL" if argument has no value (meaning there is no next argument). Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'string' (required) is an argument to get the value of. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_get_cli_argument_i1_v3') === FALSE){
function ritchey_get_cli_argument_i1_v3($string, $display_errors = NULL){
	$errors = array();
	if (@isset($string) === TRUE){
		if ($string === ''){
			$errors[] = "string";
		}
	} else {
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Search arguments for string, and get the next item.
		global $argv;
		$key = @array_search($string, $argv);
		if ($key == TRUE){
			$next_key = $key + 1;
			$value = $argv[$next_key];
		} else {
			$value = NULL;
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_get_cli_argument_i1_v3_format_error') === FALSE){
				function ritchey_get_cli_argument_i1_v3_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_get_cli_argument_i1_v3_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $value;
	} else {
		return FALSE;
	}
}
}
?>